build/Debug/Cygwin-Windows/exercicio9.o: exercicio9.cpp

#include <iostream>

using namespace std;

const int tamanho = 10;
int vetor[tamanho];
int i;

void lerVetor(int vet[],int tam){
    cout<<"digite os valores para o vetor :"<<endl;
    for (i=0;i<tam;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[],int tam){

     cout << " vetor estruturado pelo bubble sort " << endl;

    for(i =0; i < tam; i++){
        cout << "Posicao " << (i+1) <<"["<< vet[i] <<"]"<< endl;
    }
}

void bubbleSort(int vet[],int tam){
    for(i=0; i < tam; i++){
        for(int j = tam-1; j >= i ;j--){
            if(vet[j-1] > vet[j]){
                int temp = vet[j-1];
                vet[j-1] = vet[j];
                vet[j] = temp;
            }
        }
    }
}

int main()
{
    lerVetor(vetor,tamanho);
    bubbleSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);

return 0;
}