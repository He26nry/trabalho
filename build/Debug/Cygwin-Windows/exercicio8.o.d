build/Debug/Cygwin-Windows/exercicio8.o: exercicio8.cpp
#include <iostream>

using namespace std;

const int tamanho = 20;
int vetor[tamanho];
int i;

void lerVetor(int vetor[], int tam){
    for(i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": ";
        cin >> vetor[i];
    }
}

void imprimirVetor(int vetor[], int tam){
     cout << " vetor estruturado selection sort :" << endl;

    for(i =0; i < tam; i++){
        cout << "Posicao " << (i+1) <<"["<< vetor[i] <<"]"<< endl;
    }
}

void selectionSort(int vetor[], int tam){

    for(i = 0; i < tam; i++){
        int menor = i;
        for(int j =i+1; j < tam; j++){
            if(vetor[j] < vetor[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    selectionSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);
  return 0;
}